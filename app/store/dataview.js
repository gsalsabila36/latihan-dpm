Ext.define('latihan.store.dataview', {
    extend: 'Ext.data.Store',

    alias: 'store.dataview',
    storeId: 'dataview',
    /*autoLoad: true,*/

    fields: [
        'nama', 'email', 'phone'
    ],

    proxy: {
        type: 'jsonp',
        api : {
            read : "http://localhost/MyApp_php/readDataview.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
