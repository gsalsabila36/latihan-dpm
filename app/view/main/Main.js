/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('latihan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'latihan.view.main.MainController',
        'latihan.view.main.MainModel',
        'latihan.view.main.List',
        'latihan.view.dataview.BasicDataView',
        'latihan.view.main.FormPanel',
        'latihan.view.main.Bar'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Read Personnal',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onReadClicked'
                    }
                },{
                    xtype : 'spacer'
                },
                {
                    
                    xtype: 'button',
                    text: 'Read Detail',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onReadDetailClicked'
                    }
                }
            ]
        },{
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                //xtype: 'panel',
                layout: 'vbox',
                items : [
                {
                    xtype: 'mainlist',
                    flex: 2
                }/*,
                {
                    xtype: 'BasicDataView',
                    flex: 1
                },
                {
                    xtype: 'form-panel',
                    flex: 1
                }*/]
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                items : [
                {
                    xtype: 'BasicDataView',
                    flex: 4
                },
                {
                    xtype: 'spacer'
                },
                {
                    xtype: 'form-panel',
                    flex: 3
                }]
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'bar-charts'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});
