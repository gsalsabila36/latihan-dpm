/**
 * This view is an example list of people.
 */
Ext.define('latihan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

   requires: [
        'latihan.store.Personnel',
        'Ext.grid.plugin.Editable'
    ],

    plugins: [{
        type: 'grideditable'
    }],

    title: 'Data Anggota',
   /*store: {
        type: 'personnel'
    },*/
    bind : '{personnel}',
    viewModel : {
        stores : {
            personnel: {
                type: 'personnel'
            }
        }
    },

    columns: [
        { text: 'Name',  dataIndex: 'nama', width: 150, editable: true },
        { text: 'Email', dataIndex: 'email', width: 230, editable:true},
        { text: 'Phone', dataIndex: 'phone', width: 150, editable: true }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
