/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('latihan.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        /*Ext.getStore('personnel').filter('nama', record.data.nama);*/
        Ext.getStore('dataview').getProxy().setExtraParams({
            user_id: record.data.user_id
        });
        Ext.getStore('dataview').load();
        /*Ext.getStore('personnel').remove(record);
        alert("Data Telah Dihapus");*/
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    onReadClicked: function() {
        Ext.getStore('personnel').load();
    },

    onReadDetailClicked: function() {
        Ext.getStore('dataview').load();
    },

    onSimpanPerubahan: function(){
        nama = Ext.getCmp('myname').getValue();
        email =  Ext.getCmp('myemail').getValue();
        phone = Ext.getCmp('myphone').getValue();
    
        store = Ext.getStore('personnel');
        record = Ext.getCmp('mydataview').getSelection();
        index = store.indexOf(record);
        record = store.getAt(index);
        store.beginUpdate();
        record.set('nama', nama);
        record.set('email', email);
        record.set('phone', phone);
        store.endUpdate();
        alert("Updating...");
    },

    onTambahPersonnel: function(){
        nama = Ext.getCmp('myname').getValue();
        email =  Ext.getCmp('myemail').getValue();
        phone = Ext.getCmp('myphone').getValue();
    
        store = Ext.getStore('personnel');
        store.beginUpdate();
        store.insert(0, {'nama' :nama, 'email' :email, 'phone' :phone});
        store.endUpdate();
        alert("Inserting...");
    }
});

function onDeletePersonnel(user_id) {
     record = Ext.getCmp('mydataview').getSelection();
     Ext.getStore('personnel').remove(record);
     alert(user_id);   
};

function onUpdatePersonnel(user_id) {
    record = Ext.getCmp('mydataview').getSelection();
    nama = record.data.nama;
    email = record.data.email;
    phone = record.data.phone;
    Ext.getCmp('myname').setValue(nama);
    Ext.getCmp('myemail').setValue(email);
    Ext.getCmp('myphone').setValue(phone);
};
