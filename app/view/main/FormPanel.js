Ext.define('latihan.view.main.FormPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'form-panel',

    shadow: true,
    id: 'editform',
    items: [
                {
                    xtype: 'textfield',
                    name: 'nama',
                    id: 'myname',
                    label: 'Name',
                    placeHolder: 'Your Name',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    id: 'myemail',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'phone',
                    id: 'myphone',
                    label: 'Phone',
                    placeHolder: '08122222',
                    clearIcon: true
                },
                {
                    xtype: 'button',
                    ui: 'action',
                    text: 'Simpan Perubahan',
                    handler : 'onSimpanPerubahan'
                },
                {
                    xtype: 'button',
                    ui: 'confirm',
                    text: 'Tambah Personnel',
                    handler : 'onTambahPersonnel'
                }
    ]
});
